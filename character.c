/*
** RETRO GAME, Vincent PINEAU 2021
*/

#include "SDL.h"
#include "SDL_draw.h"
#include "graphic.h"
#include "character.h"

extern SDL_Surface *screen;

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void fillEllipse(int x, int y, int dx, int dy, int rx, int ry, int orientation, Uint32 color)
{
    switch (orientation)
    {
    case SDLK_DOWN:
        dy *= -1;
        break;
    case SDLK_LEFT:
        swap(&rx, &ry);
        swap(&dx, &dy);
        dy *= -1;
        break;
    case SDLK_RIGHT:
        swap(&rx, &ry);
        swap(&dx, &dy);
        dx *= -1;
        dy *= -1;
        break;
    }
    Draw_FillEllipse(screen, x + dx, y + dy, rx, ry, color);
}

void fillCircle(int x, int y, int dx, int dy, int r, int orientation, Uint32 color)
{
    switch (orientation)
    {
    case SDLK_DOWN:
        dy *= -1;
        break;
    case SDLK_LEFT:
        swap(&dx, &dy);
        dy *= -1;
        break;
    case SDLK_RIGHT:
        swap(&dx, &dy);
        dx *= -1;
        dy *= -1;
        break;
    }
    Draw_FillCircle(screen, x + dx, y + dy, r, color);
}

void rect(int x, int y, int dx, int dy, int lx, int ly, int orientation, Uint32 color)
{
    switch (orientation)
    {
    case SDLK_LEFT:
    case SDLK_RIGHT:
        swap(&dx, &dy);
        swap(&lx, &ly);
        break;
    }
    Draw_Rect(screen, x + dx, y + dy, lx, ly, color);
}

int outOfBound(int x, int y, int dx, int dy, int lx, int ly, int orientation)
{
    switch (orientation)
    {
    case SDLK_LEFT:
    case SDLK_RIGHT:
        swap(&dx, &dy);
        swap(&lx, &ly);
        break;
    }
    x+=dx;
    y+=dy;

    return (x<0 || x>MAX_X || y<0 || y>MAX_Y || (x+lx)<0 || (x+lx)>MAX_X || (y+ly)<0 || (y+ly)>MAX_Y);
}

int drawCharacter(int x, int y, int orientation)
{
    if (outOfBound(x, y, -30, -15, 30*2, 10*2+10, orientation))
        return FALSE;

    Uint32 clotheColor = SDL_MapRGB(screen->format, 12, 133, 16);
    Uint32 hairColor = SDL_MapRGB(screen->format, 133, 40, 12);
    Uint32 shoesColor = SDL_MapRGB(screen->format, 54, 16, 200);
    Uint32 skinColor = SDL_MapRGB(screen->format, 179, 172, 16);

    // shoe left
    fillEllipse(x, y, -12, -8, 5, 7, orientation, shoesColor);
    // shoe right
    fillEllipse(x, y, 12, -8, 5, 7, orientation, shoesColor);
    // left hand
    fillEllipse(x, y, -25, -2, 5, 7, orientation, skinColor);
    // right hand
    fillEllipse(x, y, 25, -2, 5, 7, orientation, skinColor);
    // body
    fillEllipse(x, y, 0, 0, 30, 10, orientation, clotheColor);
    // head
    fillCircle(x, y, 0, -2, 10, orientation, skinColor);
    // nose
    fillCircle(x, y, 0, -12, 3, orientation, skinColor);
    // hair
    fillCircle(x, y, 0, 0, 10, orientation, hairColor);
    //box

    return TRUE;
}
