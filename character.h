/*
** RETRO GAME, Vincent PINEAU 2021
*/

#ifndef CHARACTER_H
#define CHARACTER_H

void drawBackground();
int drawCharacter(int x, int y, int orientation);
#endif