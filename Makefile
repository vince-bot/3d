EXEC=game
SRC= $(wildcard *.c)
CC=gcc
DEBUG=yes
#LOG_LEVEL = 1 : error only, 2 : error and warning, 3 : error warning and info
ifeq ($(DEBUG),yes)
	CFLAGS= `sdl-config --cflags --libs` -W -Wall  -pedantic -g -D LOG_LEVEL=3
else
	CFLAGS= `sdl-config --cflags --libs` -W -Wall  -pedantic -D LOG_LEVEL=1
endif

LDFLAGS=-lSDL_draw -lwiringPi -lSDL -lpthread 

OBJ= $(SRC:.c=.o)

all: $(EXEC)
	@echo "compile game"

game: $(OBJ)
	@$(CC) -o $@ $^ $(LDFLAGS)

%.o: %.c
	@$(CC) -o $@ -c $< $(CFLAGS)

.PHONY: clean mrproper

clean:ls
	@rm -rf *.o

mrproper: clean
	@rm -rf $(EXEC)