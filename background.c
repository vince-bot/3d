/*
** RETRO GAME, Vincent PINEAU 2021
*/

#include "SDL.h"
#include "SDL_draw.h"
#include "graphic.h"
#include "background.h"

#define WALL_WIDTH 10

extern SDL_Surface *screen;

void wallV(int x, int y, int length, Uint32 color)
{
    Draw_FillRect(screen, x, y, WALL_WIDTH, length, color);
}

void wallH(int x, int y, int length, Uint32 color)
{
    Draw_FillRect(screen, x, y, length, WALL_WIDTH, color);
}

void drawBackground()
{
    Uint32 wallColor = SDL_MapRGB(screen->format, 32, 255, 255);
    wallV(0, 0, MAX_Y, wallColor);
    wallH(0, 0, MAX_X, wallColor);
    wallV(MAX_X-WALL_WIDTH, 0, MAX_Y, wallColor);
    wallH(0, MAX_Y-WALL_WIDTH, MAX_X, wallColor);
}