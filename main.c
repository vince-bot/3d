/*
** RETRO GAME, Vincent PINEAU 2021
*/

#include "game.h"
#include "graphic.h"

int main() {
    if (!initGame()) {
        gameErrorLog("Initialization error ...");
    } else {
        gameLog("Initialization ok");
        startGame();
    }
    closeGame();
}