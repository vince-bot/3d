/*
** RETRO GAME, Vincent PINEAU 2021
*/

#include <stdlib.h>
#include <pthread.h>
#include <wiringPi.h>
#include "SDL.h"

#include "game.h"

#define DOWN  1
#define UP    2
#define LEFT  3
#define RIGHT 4

#define MILI_SEC 1000000L         // 1 miliseconde
#define NODE_WAIT MILI_SEC * 10L // 10 miliseconds (0.1 second)

// init wriring pi for the gampad

int gamepadInit()
{
    gameLog("Init game pad");
    if (wiringPiSetup() == -1)
    {
        gameErrorLog("setup wiringPi failed !");
        return EXIT_FAILURE;
    }
    pinMode(DOWN, INPUT);
    pinMode(UP, INPUT);
    pinMode(LEFT, INPUT);
    pinMode(RIGHT, INPUT);
    //pull up to 3.3V,make GPIO a stable level
    pullUpDnControl(DOWN, PUD_UP);
    pullUpDnControl(UP, PUD_UP);
    pullUpDnControl(LEFT, PUD_UP);
    pullUpDnControl(RIGHT, PUD_UP);
    return EXIT_SUCCESS;
}

int isKeyDown(int key)
{
    return (key==SDLK_DOWN ||digitalRead(DOWN) == 0);
}

int isKeyUp(int key)
{
    return (key==SDLK_UP ||digitalRead(UP) == 0);
}

int isKeyLeft(int key)
{
    return (key==SDLK_LEFT || digitalRead(LEFT) == 0);
}

int isKeyRight(int key)
{
    return (key==SDLK_RIGHT || digitalRead(RIGHT) == 0);
}

void short_wait()
{
    struct timespec sleeptime = {0, (long)NODE_WAIT};
    nanosleep(&sleeptime, NULL);
}
