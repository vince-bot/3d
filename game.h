/*
** RETRO GAME, Vincent PINEAU 2021
*/

#ifndef GAME_H
#define GAME_H

int initGame();
void closeGame();
void gameLog(char *);
void gameErrorLog(char *);

#endif