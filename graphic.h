/*
** RETRO GAME, Vincent PINEAU 2021
*/

#ifndef GRAPHIC_H
#define GRAPHIC_H

#define MAX_X 1024
#define MAX_Y 768

#ifndef	TRUE
#  define	TRUE	(1==1)
#  define	FALSE	(!TRUE)
#endif

int graphicInit();
void startGame();

#endif