/*
** RETRO GAME, Vincent PINEAU 2021
*/

#ifndef GAMPEPAD_H
#define GAMPEPAD_H

int gamepadInit();
int isKeyDown(int key);
int isKeyUp(int key);
int isKeyLeft(int key);
int isKeyRight(int key);
void short_wait();

#endif