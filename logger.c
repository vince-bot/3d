/*
** RETRO GAME, Vincent PINEAU 2021
*/

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "logger.h"

void handle_error(const char *fmt, ...)
{
    int size = 0;
    char *p = NULL;
    va_list ap;

    /* Determine required size */

    va_start(ap, fmt);
    size = vsnprintf(p, size, fmt, ap);
    va_end(ap);

    if (size < 0)
        exit(EXIT_FAILURE);

    size++; /* For '\0' */
    p = malloc(size);
    if (p == NULL)
        exit(EXIT_FAILURE);

    va_start(ap, fmt);
    size = vsnprintf(p, size, fmt, ap);
    va_end(ap);

    if (size < 0)
        free(p);

    perror(p);
    exit(EXIT_FAILURE);
}

t_log *create_log(char *name)
{
    t_log *log_file = malloc(sizeof(t_log));
    if (log_file == NULL)
        handle_error("malloc");
    log_file->name = calloc(strlen(name) + 1, sizeof(char));
    if (log_file->name == NULL)
        handle_error("calloc");
    strcpy(log_file->name, name);
    char *dir = "./log/";
    char *ext = ".log";
    char *path = calloc(strlen(dir) + strlen(name) + strlen(ext) + 1, sizeof(char));
    if (log_file->name == NULL)
        handle_error("calloc");
    strcpy(path, dir);
    strcat(path, name);
    strcat(path, ext);
    log_file->p_file = fopen(path, "w");
    if (log_file->p_file == NULL)
        handle_error("Cannot open file %s", path);
    free(path);
    return log_file;
}

int write_log(t_log *log_file, int level, int screen, char *mess)
{
    char txt_level[5];
    if (level > LOG_LEVEL)
        return EXIT_SUCCESS;

    switch (level)
    {
    case LEVEL_ERROR:
        strcpy(txt_level, "ERRO");
        break;
    case LEVEL_WARNING:
        strcpy(txt_level, "WARN");
        break;
    case LEVEL_INFO:
        strcpy(txt_level, "INFO");
        break;
    }

    if (log_file->p_file != NULL)
    {
        fprintf(log_file->p_file, "%s %s", txt_level, mess);
        fprintf(log_file->p_file, "\n");
        fflush(log_file->p_file);
    }
    if (screen || (log_file->p_file == NULL))
        printf("%s %s\n", txt_level, mess);

    return EXIT_SUCCESS;
}

int close_log(t_log *log_file)
{
    fclose(log_file->p_file);
    free(log_file->name);
    return EXIT_SUCCESS;
}
