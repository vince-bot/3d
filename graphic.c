#include <stdlib.h>

#include "SDL.h"
#include "SDL_draw.h"
#include "graphic.h"
#include "gamepad.h"
#include "game.h"
#include "background.h"
#include "character.h"

SDL_Surface *screen;

int graphicInit()
{
    Uint8 video_bpp;
    Uint32 videoflags;

    gameLog("Init graphic mode");
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        gameErrorLog("Error during sdl init :");
        gameErrorLog(SDL_GetError());
        return EXIT_FAILURE;
    }

    atexit(SDL_Quit);
    videoflags = SDL_SWSURFACE | SDL_ANYFORMAT;
    video_bpp = 0;

    /*Video mode activation*/
    screen = SDL_SetVideoMode(MAX_X, MAX_Y, video_bpp, videoflags);
    if (!screen)
    {
        gameErrorLog("I can not activate video mode :");
        gameErrorLog(SDL_GetError());
        return EXIT_FAILURE;
    }

    SDL_ShowCursor(SDL_DISABLE);
    return EXIT_SUCCESS;
}

int checkEvent()
{
    SDL_Event event;
    while (SDL_PollEvent(&event))
        if (event.type == SDL_KEYDOWN)
        {
            if (event.key.keysym.sym == SDLK_SPACE)
                return -1;
            else
                return event.key.keysym.sym;
        }
    return 0;
}

void clearScreen()
{
    SDL_FillRect(screen, NULL, 0);
}

void startGame()
{
    int key = 0;
    int orientation = SDLK_UP;
    int orientationSvg, xSvg, ySvg;
    int x = MAX_X / 2;
    int y = MAX_Y / 2;
    int stepX = 5;
    int stepY = 5;

    while (key != -1)
    {
        orientationSvg = orientation;
        xSvg = x;
        ySvg = y;
        key = checkEvent();
        if (isKeyDown(key))
        {
            orientation = SDLK_DOWN;
            y += stepY;
        }
        if (isKeyUp(key))
        {
            orientation = SDLK_UP;
            y -= stepY;
        }
        if (isKeyLeft(key))
        {
            orientation = SDLK_LEFT;
            x -= stepX;
        }
        if (isKeyRight(key))
        {
            orientation = SDLK_RIGHT;
            x += stepX;
        }
        clearScreen();
        drawBackground();
        if (!drawCharacter(x, y, orientation))
        {
            orientation = orientationSvg;
            x = xSvg;
            y = ySvg;
        }
        else
            SDL_Flip(screen);
        short_wait();
    }
}