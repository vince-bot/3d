/*
** RETRO GAME, Vincent PINEAU 2021
*/

#ifndef LOG_SYSTEM_H
#define LOG_SYSTEM_H

#include <stdio.h>
#include <time.h>
//log level
#define LEVEL_ERROR   1
#define LEVEL_WARNING 2
#define LEVEL_INFO    3
//print log on screen
#define FILE_ONLY 0
#define ON_SCREEN 1

struct s_log {
    FILE *p_file;
    char *name;
};

typedef struct s_log t_log;

void handle_error(const char *fmt, ...);
t_log *create_log(char *name);
int write_log(t_log *log_file, int level, int screen, char *mess);
int close_log(t_log *log_file);
#endif