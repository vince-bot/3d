/*
** RETRO GAME, Vincent PINEAU 2021
*/
#include <stdlib.h>

#include "logger.h"
#include "game.h"
#include "gamepad.h"
#include "graphic.h"

t_log *main_log_file;

int initGame()
{
    main_log_file = create_log("main");
    gameLog("Game is starting !");
    if (gamepadInit())
    {
        gameLog("Game cannot be initialized !");
        return EXIT_FAILURE;
    }
    if (graphicInit())
    {
        gameLog("Game cannot be initialized !");
        return EXIT_FAILURE;
    }
    return EXIT_FAILURE;
}

void closeGame()
{
    gameLog("Game is done !");
    close_log(main_log_file);
}

void gameLog(char *mess)
{
    write_log(main_log_file, LEVEL_INFO, FILE_ONLY, mess);
}

void gameErrorLog(char *mess)
{
    write_log(main_log_file, LEVEL_ERROR, FILE_ONLY, mess);
}
